package Negocio;
import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * The test class SopaDeLetrasTest.
 *
 * @author  (your name)
 * @version (a version number or a date)
 */
public class SopaDeLetraTest
{
    /**
     * Default constructor for test class SopaDeLetrasTest
     */
    public SopaDeLetraTest()
    {
    }

    /**
     * Sets up the test fixture.
     *
     * Called before every test case method.
     */
    @Before
    public void setUp()
    {
    }

    /**
     * Tears down the test fixture.
     *
     * Called after every test case method.
     */
    @After
    public void tearDown()
    { 
        
    }
    
    @Test
    public void crearMatriz() throws Exception
    {
    String palabras="marc,pe,a";
    SopaDeLetra s=new SopaDeLetra(palabras);
    
    char ideal[][]={{'m','a','r','c'},{'p','e'},{'a'}};
    
    char esperado[][]=s.getSopa();
    // :) --> ideal == esperado
    
    boolean pasoPrueba=sonIguales(ideal, esperado);
    assertEquals(true,pasoPrueba);
    
    }
    @Test
    public void crearMatriz2() throws Exception
    {
    String palabras="duba,si,b";
    SopaDeLetra s=new SopaDeLetra(palabras);
    
    char ideal[][]={{'d','u','b','a'},{'s','i'},{'b'}};
    
    char esperado[][]=s.getSopa();
    // :) --> ideal == esperado
    
    boolean pasoPrueba=sonIguales(ideal, esperado);
    assertEquals(true,pasoPrueba);
    
    }
    @Test
    public void crearMatriz3() throws Exception
    {
    String palabras="jesu,an,v";
    SopaDeLetra s=new SopaDeLetra(palabras);
    
    char ideal[][]={{'j','e','s','u'},{'a','n'},{'v'}};
    
    char esperado[][]=s.getSopa();
    // :) --> ideal == esperado
    
    boolean pasoPrueba=sonIguales(ideal, esperado);
    assertEquals(true,pasoPrueba);
    
    }
    
    
    @Test
    public void crearMatrizConError() throws Exception
    {
    String palabras="marc,pe,a";
    SopaDeLetra s=new SopaDeLetra();
    
    char idealError[][]={{'m','a','r','c'},{'p','e'},{'a'}};
    char esperado[][]=s.getSopa();
    // :) --> ideal == esperado
    
    boolean pasoPrueba=sonIguales(idealError, esperado);
    assertEquals(false,pasoPrueba);
    
    }
    
        @Test
    public void crearMatrizConError2() throws Exception
    {
    String palabras="uno,do,t";
    SopaDeLetra s=new SopaDeLetra();
    
    char idealError[][]={{'u','n','o'},{'d','o'},{'t'}};
    char esperado[][]=s.getSopa();
    // :) --> ideal == esperado
    
    boolean pasoPrueba=sonIguales(idealError, esperado);
    assertEquals(false,pasoPrueba);
    
    }

    
    @Test
    public void testCrearMatrizCuadrada() throws Exception
    {
        String prueba="luis,luis,luis,luis";
        SopaDeLetra s=new SopaDeLetra(prueba);
        boolean paso= s.esCuadrada();
        assertEquals(true,paso);
    }
    
    @Test
    public void testCrearMatrizCuadrada2() throws Exception
    {
        String prueba="je,du";
        SopaDeLetra s=new SopaDeLetra(prueba);
        boolean paso= s.esCuadrada();
        assertEquals(true,paso);
    }
    
        @Test
    public void testCrearMatrizCuadrada3() throws Exception
    {
        String prueba="pai,bro,man";
        SopaDeLetra s=new SopaDeLetra(prueba);
        boolean paso= s.esCuadrada();
        assertEquals(true,paso);
    }
    
          @Test
    public void testCrearMatrizCuadradaError() throws Exception
    {
        String prueba="bes,ode,tres";
        SopaDeLetra s=new SopaDeLetra(prueba);
        boolean paso= s.esCuadrada();
        assertEquals(false,paso);
    }
    
          @Test
    public void testCrearMatrizCuadradaError2() throws Exception
    {
        String prueba="erad,ecua,troo";
        SopaDeLetra s=new SopaDeLetra(prueba);
        boolean paso= s.esCuadrada();
        assertEquals(false,paso);
    }
    
        @Test
    public void testCrearMatrizRectangular() throws Exception
    {
        String prueba="jen,alb";
        SopaDeLetra s=new SopaDeLetra(prueba);
        boolean paso= s.esRectangular();
        assertEquals(true,paso);
    }
    
            @Test
    public void testCrearMatrizRectangular2() throws Exception
    {
        String prueba="pato,palo,elio,agua,hola";
        SopaDeLetra s=new SopaDeLetra(prueba);
        boolean paso= s.esRectangular();
        assertEquals(true,paso);
    }
    
            @Test
    public void testCrearMatrizRectangular3() throws Exception
    {
        String prueba="d,j";
        SopaDeLetra s=new SopaDeLetra(prueba);
        boolean paso= s.esRectangular();
        assertEquals(true,paso);
    }
    
                @Test
    public void testCrearMatrizRectangularError() throws Exception
    {
        String prueba="elio,aire,oro";
        SopaDeLetra s=new SopaDeLetra(prueba);
        boolean paso= s.esRectangular();
        assertEquals(false,paso);
    }
    
                @Test
    public void testCrearMatrizRectangularError2() throws Exception
    {
        String prueba="da,je";
        SopaDeLetra s=new SopaDeLetra(prueba);
        boolean paso= s.esRectangular();
        assertEquals(false,paso);
    }
    
                @Test
    public void testCrearMatrizDispersa() throws Exception
    {
        String prueba="semja,voj,drazah";
        SopaDeLetra s=new SopaDeLetra(prueba);
        boolean paso= s.esDispersa();
        assertEquals(true,paso);
    }
    
                @Test
    public void testCrearMatrizDispersa2() throws Exception
    {
        String prueba="james,hazard,jov";
        SopaDeLetra s=new SopaDeLetra(prueba);
        boolean paso= s.esDispersa();
        assertEquals(true,paso);
    }
    
                @Test
    public void testCrearMatrizDispersa3() throws Exception
    {
        String prueba="ney,messi,cris";
        SopaDeLetra s=new SopaDeLetra(prueba);
        boolean paso= s.esDispersa();
        assertEquals(true,paso);
    }
    
                    @Test
    public void testCrearMatrizDispersaError() throws Exception
    {
        String prueba="je,da";
        SopaDeLetra s=new SopaDeLetra(prueba);
        boolean paso= s.esDispersa();
        assertEquals(false,paso);
    }
    
                    @Test
    public void testCrearMatrizDispersaError2() throws Exception
    {
        String prueba="ca,mi";
        SopaDeLetra s=new SopaDeLetra(prueba);
        boolean paso= s.esDispersa();
        assertEquals(false,paso);
    }
    /*
    @Test
    public void testCrearMatrizDiagonalPrincipal() throws Exception
    {
        String prueba="mama,mama,mama,mama";
        SopaDeLetra s=new SopaDeLetra(prueba);
        char diagonalIdeal[] = {'m','a','m','a'};
        char rta2[] = s.getDiagonalPrincipal();
        boolean paso=sonIgualesVector(rta2,diagonalIdeal);
        assertEquals(true,paso);
    }
    
        @Test
    public void testCrearMatrizDiagonalPrincipal2() throws Exception
    {
        String prueba="sol,ana,del";
        SopaDeLetra s=new SopaDeLetra(prueba);
        char diagonalIdeal[] = {'s','n','l'};
        char rta2[] = s.getDiagonalPrincipal();
        boolean paso=sonIgualesVector(rta2,diagonalIdeal);
        assertEquals(true,paso);
    }
    
        @Test
    public void testCrearMatrizDiagonalPrincipal3() throws Exception
    {
        String prueba="je,su";
        SopaDeLetra s=new SopaDeLetra(prueba);
        char diagonalIdeal[] = {'j','u'};
        char rta2[] = s.getDiagonalPrincipal();
        boolean paso=sonIgualesVector(rta2,diagonalIdeal);
        assertEquals(true,paso);
    }
    
            @Test
    public void testCrearMatrizDiagonalPrincipalError() throws Exception
    {
        String prueba="lol,wow,ama";
        SopaDeLetra s=new SopaDeLetra(prueba);
        char diagonalIdeal[] = {'l','o','m'};
        char rta2[] = s.getDiagonalPrincipal();
        boolean paso=sonIgualesVector(rta2,diagonalIdeal);
        assertEquals(false,paso);
    }
    
            @Test
    public void testCrearMatrizDiagonalPrincipalError2() throws Exception
    {
        String prueba="ej,us";
        SopaDeLetra s=new SopaDeLetra(prueba);
        char diagonalIdeal[] = {'j','u'};
        char rta2[] = s.getDiagonalPrincipal();
        boolean paso=sonIgualesVector(rta2,diagonalIdeal);
        assertEquals(false,paso);
    }
    */
    @Test
    public void testDiagonalInferior() throws Exception {
     String prueba="pepe,pepe,pepe,pepe";
     SopaDeLetra s=new SopaDeLetra(prueba);
     char diagonal [] = s.getDiagonalInferior();
     char esperada[] = {'e','p','e','p','e','e'};
     boolean esIgual = sonIgualesVector(diagonal,esperada);
     assertEquals(true,esIgual);
    }
    
    @Test
    public void testDiagonalInferior2() throws Exception {
     String prueba="anama,marco,pepes,juane,andre";
     SopaDeLetra s=new SopaDeLetra(prueba);
     char diagonal [] = s.getDiagonalInferior();
     char esperada[] = {'n','a','m','a','r','c','o','e','s','e'};
     boolean esIgual = sonIgualesVector(diagonal,esperada);
     assertEquals(true,esIgual);
    }
    
    @Test
    public void testDiagonalInferior3() throws Exception {
     String prueba="josnes,martas,julian,marlay,camilo,andres";
     SopaDeLetra s=new SopaDeLetra(prueba);
     char diagonal [] = s.getDiagonalInferior();
     char esperada[] = {'o','s','n','e','s','r','t','a','s','i','a','n','a','y','o'};
     boolean esIgual = sonIgualesVector(diagonal,esperada);
     assertEquals(true,esIgual);
    }
    
    @Test
    public void testDiagonalInferiorError() throws Exception {
     String prueba="duy,yes,sil";
     SopaDeLetra s=new SopaDeLetra(prueba);
     char diagonal [] = s.getDiagonalInferior();
     char esperada[] = {'d','e','y'};
     boolean esIgual = sonIgualesVector(diagonal,esperada);
     assertEquals(false,esIgual);
    }
   
    @Test
    public void testDiagonalInferiorError2() throws Exception {
     String prueba="duba,yesi,silv,buit";
     SopaDeLetra s=new SopaDeLetra(prueba);
     char diagonal [] = s.getDiagonalInferior();
     char esperada[] = {'b','d','a','t','s','l'};
     boolean esIgual = sonIgualesVector(diagonal,esperada);
     assertEquals(false,esIgual);
    }
    @Test
    public void testLetraMasRepetida() throws Exception
    {
        String prueba="marco,pepe,anamaria";
        SopaDeLetra s=new SopaDeLetra(prueba);
        char letra = 'a';
        char paso = s.masSeRepite();
        assertEquals(letra,paso);
    }
    @Test
    public void testLetraMasRepetida2() throws Exception
    {
        String prueba="marco,pepe,anmaria";
        SopaDeLetra s=new SopaDeLetra(prueba);
        char letra = 'a';
        char paso = s.masSeRepite();
        assertEquals(letra,paso);
    }
    @Test
    public void testLetraMasRepetida3() throws Exception
    {
        String prueba="marco,pepe,felipe";
        SopaDeLetra s=new SopaDeLetra(prueba);
        char letra = 'e';
        char paso = s.masSeRepite();
        assertEquals(letra,paso);
    }
    @Test
    public void testLetraMasRepetidaError() throws Exception
    {
        String prueba="marco,pepe,pipo";
        SopaDeLetra s=new SopaDeLetra(prueba);
        char paso = s.masSeRepite();
        char letra = 'a';
        boolean igual = (paso == letra);
        assertEquals(false,igual);
    }@Test
    public void testLetraMasRepetidaError2() throws Exception
    {
        String prueba="duban,bb,b";
        SopaDeLetra s=new SopaDeLetra(prueba);
       char paso = s.masSeRepite();
        char letra = 'a';
        boolean igual = (paso == letra);
        assertEquals(false,igual);
    }
    
    private boolean sonIguales(char m1[][], char m2[][])
    {
        
        if(m1==null || m2==null || m1.length !=m2.length)
            return false; 
            
      int cantFilas=m1.length; 
      
      for(int i=0;i<cantFilas;i++)
      {
          if( !sonIgualesVector(m1[i],m2[i]))
            return false;
      }
      
        
    return true;
    }
    
    private boolean sonIgualesVector(char v1[], char v2[])
    {
        if(v1.length != v2.length)
                return false;
         
        for(int j=0;j<v1.length;j++)
      {
            if(v1[j]!=v2[j])
                return false;
      }
      return true;
    }
}