package Negocio;

/**
 * Write a description of class SopaLetra here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class SopaDeLetra
{
    // La matriz con las letras de la sopa
    private char sopa[][];

    /**
     * Constructor for objects of class SopaLetras
     */
    public SopaDeLetra()
    {

    }

    public SopaDeLetra(String palabras) throws Exception
    {
        if(palabras==null || palabras.isEmpty())
        {
            throw new Exception("No se puede crear la sopa");
        }

        String palabras2[]=palabras.split(",");
        //Crear la cantidad de filas:
        this.sopa=new char[palabras2.length][];
        //recorrer cada elemento de palabras2 y pasarlo a su correspondiente fila en sopa:
        int i=0;
        for(String palabraX: palabras2)
        {
            this.sopa[i]=new char[palabraX.length()];
            pasar(palabraX, this.sopa[i]);
            i++;
        }

    }

    private void pasar(String x, char vector[])
    {
        for(int j=0;j<x.length();j++)
            vector[j]=x.charAt(j);
    }

    public String toString()
    {
        String msg="";
        for(int i=0;i<this.sopa.length;i++)
        {
            for (int j=0;j<this.sopa[i].length;j++)
            {
                msg+=this.sopa[i][j]+""+"\t";
            }
            msg+="\n";
        }
        return msg;
    }

    public String toString2()
    {
        String msg="";
        for(char vector[]:this.sopa)
        {
            for (char dato:vector)
            {
                msg+=dato+""+"\t";
            }
            msg+="\n";
        }
        return msg;
    }

    /**
    Retorna la letra que más se repite
     */
    public char masSeRepite()
    {
        int numRepite[]=new int[26];
        int index=0;
        for (int i = 0; i < this.sopa.length ; i++) {
            for (int j = 0; j < this.sopa[i].length ; j++) {
                int pos=sopa[i][j]-97;
                numRepite[pos]++;
                if(numRepite[index]<numRepite[pos]){
                    index=pos;

                }
            }
        }
        int ret=index+97;
        return (char)ret;
    }    

    public boolean esCuadrada()
    {
        boolean esCuadrada = true;
        int c = this.sopa.length;
        for (int i = 0; i < this.sopa.length ; i++) {
            if (c != this.sopa[i].length) 
                esCuadrada = false;            
            //if(this.sopa.length==sopa[0].length){ 
            // esCuadrada=true;
            //}

        }
        return  esCuadrada;
    } 
    public boolean esRectangular()
    {
        boolean esRectangular = true;
        for (int i = 0; i < sopa.length && esRectangular; i++) {
            if (sopa.length == sopa[i].length) {
                esRectangular = false;
            }
        }
        return esRectangular;
    }

    public boolean esDispersa()
    {  
        return (!this.esRectangular()&&!this.esCuadrada());
    }

    /*public char []getDiagonalPrincipal() throws Exception
    {
        //si y solo si es cuadrada , si no, lanza excepcion
        if(!esCuadrada())
        {
            throw new Exception("Error, no es cuadrada.");
        }
        char diagoPrincipal[] = new char[sopa.length];
        for (int i = 0; i < this.sopa.length; i++) {
            for (int j = 0; j < sopa[i].length; j++) {
                if (i == j) {
                    diagoPrincipal[i] = sopa[i][j];
                }
            }
        }
        return diagoPrincipal;
    }*/
    public char []getDiagonalInferior() throws Exception
    {
        char diagonalInferior[];
        //si y solo si es cuadrada , si no, lanza excepcion
        if(!esCuadrada()){
        throw new Exception("La matriz no es cuadrada");
        }
        int tam = 0;
        int filas  =  this.sopa.length;
        for(int i=0; i < filas ; i++){
            tam = tam + (filas-i) - 1;
        }
        diagonalInferior= new char [tam];
        int a = 0;
        for(int i=0; i<this.sopa.length ;i++){
            for (int j=i;j<this.sopa[i].length-1;j++)
            {
                diagonalInferior[a]= sopa[i][j+1];
                a++;
            }
        }
        return diagonalInferior;
    }
    //Start GetterSetterExtension Source Code
    /**GET Method Propertie sopa*/
    public char[][] getSopa(){
        return this.sopa;
    }//end method getSopa

    /**SET Method Propertie sopa*/
    public void setSopa(char[][] sopa){
        this.sopa = sopa;
    }//end method setSopa

    //End GetterSetterExtension Source Code
}//End class !