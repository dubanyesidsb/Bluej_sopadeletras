package Vista;
import Negocio.SopaLetra;

/**
 * Write a description of class TestSopa here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class TestSopa
{
    public static void main(String nada[])
    {
        try{
            SopaLetra b=new SopaLetra("marco,pepe,ana maria");         //Crea Sopa de Letra
            System.out.println("cuadrada:" + b.esCuadrada());         //Probar Metodo Cuadrada
            System.out.println("rectangular:" + b.esRectangular());      //Probar Metodo Rectangular
            System.out.println("dispersa:" + b.esDispersa());         //Probar Metodo Dispersa
            //System.out.println(b.getMas_SeRepite());    //Probar Metodo Mas se Repite
            char diagonal[] = b.getDiagonalInferior();
            for(char x : diagonal ){                    //For each para recorrer la Matriz e imprimir la Diagonal Inferior 
                System.out.println(x);
            }
            System.out.println("Mas se repite "+b.masSeRepite()); 
        }catch(Exception e)
        {
            System.out.println(e.getMessage());
        }
        
    }
}
